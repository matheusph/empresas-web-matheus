 (function(){
 
 
 
 
	var app = angular.module('app', ['ngRoute',]);
	var page = 'login';
	
	
	app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
		.when("/", {templateUrl: "pages/login.html", controller: "LoginCtrl"})
        .when("/login", {templateUrl: "pages/login.html", controller: "LoginCtrl"})
        .when("/home", {templateUrl: "pages/home.html", controller: "HomeCtrl"})
        .otherwise("/404", {templateUrl: "templates/404.html"});
}]);
	
	
	
	
var userInfo={access_token:'',client:'',uid:''};	
app.controller('LoginCtrl', function ($window, $scope, $http, $location) {
	this.user={};
	$scope.login = function(){
		
		$http({
		 method: 'POST',
		 url: 'http://54.94.179.135:8090/api/v1/users/auth/sign_in',
		 data: {email: this.user.email , password: this.user.password }
		}).then(function successCallback(response) {
			
			userInfo.access_token=response.headers()['access-token'] ;
			userInfo.client=response.headers()['client'];
			userInfo.uid=response.headers()['uid'];
			$window.location.href = '#!/home';
			
		  }, function errorCallback(response) {
			  
			  
		  });
	}
	
	
	
	
	
});
app.controller('HomeCtrl', function ($scope,$http,  $location) {
	this.data=userInfo;
	this.input_search=false;
	this.type_selected="matheus";
		
		  $(document).ready(function() {
			$('select').material_select();
		  });
		$http({
		 method: 'GET',
		 url: 'http://54.94.179.135:8090/api/v1/enterprise_types',
		 headers:{
			 'access-token':userInfo.access_token,
			 'client':userInfo.client,
			 'uid':userInfo.uid
		 }
		}).then(function successCallback(response) {
			
			$scope.types=response.data;
			
			$('select').material_select();
	
		  
		  }, function errorCallback(response) {
			  
			  
		  });
		  
		  
	$scope.search = function(){
		
		$scope.enterprise_selected=false;
		$(".base_box1").css("height", "180px");
		 
		 let url='';
		 
		 if($('select').val()!=null&&$('select').val()!='all')
		 {
			 url='&enterprise_types='+$('select').val();
		 }
		 
		$http({
		 method: 'GET',
		 url: 'http://54.94.179.135:8090/api/v1/enterprises?name='+this.name_enterprise+url,
		 headers:{
			 'access-token':userInfo.access_token,
			 'client':userInfo.client,
			 'uid':userInfo.uid
		 }
		}).then(function successCallback(response) {
			
			$scope.enterprises=response.data.enterprises;
			
		  }, function errorCallback(response) {
			  
			  
		  });
	}
	
		  
		  $('select').on('change', function() {
			  $scope.search();
			});
		  
	
	$scope.selectEnterprise = function(enterprise){
		$scope.enterprises = [enterprise];
		$scope.name_enterprise=enterprise.enterprise_name;
		$scope.enterprise_selected=true;
		$(".base_box1").css("height", "auto");
	}
	
			
});
	
	
	
	
	
 })();